import sys
import numpy as np
import pandas as pd
import datetime
import time
import joblib
import socket

from flask import Flask, request,render_template,url_for

import os
import matplotlib.pyplot as plt
import argparse

app = Flask(__name__)
app.secret_key = 'ba35ce93c732955e6cd8ae3832b0bbb6'


#app.config["DEBUG"] = True
#app.config['UPLOAD_FOLDER'] = os.path.join('static', 'img')
#gdlogoimagepath = os.path.join(app.config['UPLOAD_FOLDER'], 'GlobalData_New-Logo.jpg')


@app.route('/', methods=['GET'])
@app.route('/home', methods=['GET'])
def home():
    return render_template('home.html',
                           title='Home')

@app.route('/about', methods=['GET'])
def about():
    return render_template('about.html',
                           title='About')

@app.route('/contentset', methods=['POST','GET'])
def contentset():
    if request.method == 'POST':
        contentset=request.form['ic_form_value']
        print(contentset)
        users=list(filter(lambda r:r[0]==contentset,users_by_icndcontentset))
        return render_template('user_selection.html', contentset=contentset,users=users)

@app.route('/recommends/<contentset>', methods=['POST','GET'])
def recommends(contentset):

    start = time.time()
    print(request.remote_addr)
    users = list(filter(lambda r: r[0] == contentset, users_by_icndcontentset))

    print("Total no.of users for this combination of IC and Content set:", len(users))
    with open(r"logs\inputs_log.txt", "a", encoding="utf-8") as f:
        f.write("_" * 100)
        f.write("\nip_address: %s \n"
                "via port: %s\n"
                "datetime: %s \n"
                "IC/Content set selected: %s \n"
                "Total no.of users for this combination of IC and Content set: %s \n"\
                % (request.remote_addr,
                   port,
                   datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                   contentset,
                   len(users)))
        f.write("--" * 100)

    if request.method == 'POST':
        print(contentset)
        icname, content = contentset.split("_")[0], contentset.split("_")[1]
        print(icname, content)
        user_id_of_interest = int(request.form['user'].split("_")[0])
        print(user_id_of_interest)
        user_name_of_interest = request.form['user'].split("_")[1]
        print(user_name_of_interest)

        view_columns = ['ICName',
                        'UserProfileId',
                        'User',
                        'CompanyName',
                        'Content Type',
                        'Viewed Date',
                        'Record Id',
                        'Record Title',
                        'Publication date']

        if content=='news':

            if icname=='consumer':

                user_views = user_viewed_records(consumer_news_df, user_id_of_interest, view_columns)

                user_viewed_records_df, user_viewed_records_list = user_views[0],user_views[1]

                print(len(user_viewed_records_df), len(user_viewed_records_list))

                ui_msg_and_recommendations = user_item_filtering_based_recommendations(consumer_news_user_item_filtering_df,
                                                                                       consumer_news_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_users_cut_off)

                ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]

                ii_msg_and_recommendations = item_item_filtering_based_recommendations(consumer_news_item_item_filtering_df,
                                                                                       consumer_news_df,
                                                                                       consumer_news_with_metadata_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_items_cut_off)

                ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]


            if icname=='construction':

                user_views = user_viewed_records(construction_news_df, user_id_of_interest, view_columns)

                user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                ui_msg_and_recommendations = user_item_filtering_based_recommendations(construction_news_user_item_filtering_df,
                                                                                       construction_news_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_users_cut_off)

                ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]

                ii_msg_and_recommendations = item_item_filtering_based_recommendations(construction_news_item_item_filtering_df,
                                                                                       construction_news_df,
                                                                                       construction_news_with_metadata_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_items_cut_off)

                ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]

            if icname=='pharma':

                user_views = user_viewed_records(pharma_news_df, user_id_of_interest, view_columns)

                user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                ui_msg_and_recommendations = user_item_filtering_based_recommendations(pharma_news_user_item_filtering_df,
                                                                                       pharma_news_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_users_cut_off)

                ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0],  ui_msg_and_recommendations[1]

                ii_msg_and_recommendations = item_item_filtering_based_recommendations(pharma_news_item_item_filtering_df,
                                                                                       pharma_news_df,
                                                                                       pharma_news_with_metadata_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_items_cut_off)

                ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]

        elif content == 'reports':

                if icname == 'consumer':
                    user_views = user_viewed_records(consumer_reports_df, user_id_of_interest, view_columns)

                    user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                    print(len(user_viewed_records_df), len(user_viewed_records_list))

                    ui_msg_and_recommendations = user_item_filtering_based_recommendations(consumer_reports_user_item_filtering_df,
                                                                                           consumer_reports_df,
                                                                                           user_viewed_records_list,
                                                                                           user_id_of_interest,
                                                                                           similar_users_cut_off)

                    ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]

                    ii_msg_and_recommendations = item_item_filtering_based_recommendations(consumer_reports_item_item_filtering_df,
                                                                                           consumer_reports_df,
                                                                                           consumer_reports_with_metadata_df,
                                                                                           user_viewed_records_list,
                                                                                           user_id_of_interest,
                                                                                           similar_items_cut_off)

                    ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]

                if icname == 'construction':
                    user_views = user_viewed_records(construction_reports_df, user_id_of_interest, view_columns)

                    user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                    ui_msg_and_recommendations = user_item_filtering_based_recommendations(construction_reports_user_item_filtering_df,
                                                                                           construction_reports_df,
                                                                                           user_viewed_records_list,
                                                                                           user_id_of_interest,
                                                                                           similar_users_cut_off)

                    ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]


                    ii_msg_and_recommendations = item_item_filtering_based_recommendations(construction_reports_item_item_filtering_df,
                                                                                           construction_reports_df,
                                                                                           construction_reports_with_metadata_df,
                                                                                           user_viewed_records_list,
                                                                                           user_id_of_interest,
                                                                                           similar_items_cut_off)

                    ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]

                if icname == 'pharma':
                    user_views = user_viewed_records(pharma_reports_df, user_id_of_interest, view_columns)

                    user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                    ui_msg_and_recommendations = user_item_filtering_based_recommendations(pharma_reports_user_item_filtering_df,
                                                                                           pharma_reports_df,
                                                                                           user_viewed_records_list,
                                                                                           user_id_of_interest,
                                                                                           similar_users_cut_off)

                    ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]


                    ii_msg_and_recommendations = item_item_filtering_based_recommendations(pharma_reports_item_item_filtering_df,
                                                                                           pharma_reports_df,
                                                                                           pharma_reports_with_metadata_df,
                                                                                           user_viewed_records_list,
                                                                                           user_id_of_interest,
                                                                                           similar_items_cut_off)

                    ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]

        elif content == 'deals':

            if icname == 'consumer':

                user_views = user_viewed_records(consumer_deals_df, user_id_of_interest, view_columns)

                user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                print(len(user_viewed_records_df), len(user_viewed_records_list))

                ui_msg_and_recommendations = user_item_filtering_based_recommendations(consumer_deals_user_item_filtering_df,
                                                                                       consumer_deals_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_users_cut_off)

                ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]

                ii_msg_and_recommendations = item_item_filtering_based_recommendations(consumer_deals_item_item_filtering_df,
                                                                                       consumer_deals_df,
                                                                                       consumer_deals_with_metadata_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_items_cut_off)

                ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]

            if icname == 'construction':

                user_views = user_viewed_records(construction_deals_df, user_id_of_interest, view_columns)

                user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                ui_msg_and_recommendations = user_item_filtering_based_recommendations(construction_deals_user_item_filtering_df,
                                                                                       construction_deals_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_users_cut_off)

                ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]

                ii_msg_and_recommendations = item_item_filtering_based_recommendations(construction_deals_item_item_filtering_df,
                                                                                       construction_deals_df,
                                                                                       construction_deals_with_metadata_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_items_cut_off)

                ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]

            if icname == 'pharma':

                user_views = user_viewed_records(pharma_deals_df, user_id_of_interest, view_columns)

                user_viewed_records_df, user_viewed_records_list = user_views[0], user_views[1]

                ui_msg_and_recommendations = user_item_filtering_based_recommendations(pharma_deals_user_item_filtering_df,
                                                                                       pharma_deals_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_users_cut_off)

                ui_msg_to_carry, user_item_filtering_recommendations_df = ui_msg_and_recommendations[0], ui_msg_and_recommendations[1]

                ii_msg_and_recommendations = item_item_filtering_based_recommendations(pharma_deals_item_item_filtering_df,
                                                                                       pharma_deals_df,
                                                                                       pharma_deals_with_metadata_df,
                                                                                       user_viewed_records_list,
                                                                                       user_id_of_interest,
                                                                                       similar_items_cut_off)

                ii_msg_to_carry, item_item_filtering_recommendations_df = ii_msg_and_recommendations[0], ii_msg_and_recommendations[1]



        else:
            return 'Something out off inputs designed'

        return render_template('recommendations.html',
                               user_name_of_interest=user_name_of_interest,
                               user_id_of_interest=user_id_of_interest,
                               contentset=contentset,
                               user_viewed_records_df=user_viewed_records_df,
                               ui_msg_to_carry=ui_msg_to_carry,
                               user_item_filtering_recommendations_df=user_item_filtering_recommendations_df,
                               ii_msg_to_carry=ii_msg_to_carry,
                               item_item_filtering_recommendations_df=item_item_filtering_recommendations_df,
                               total_time=(time.time()-start))

if __name__ == '__main__':

    hostname = socket.gethostname()
    host = socket.gethostbyname(hostname)
    print(host)

    print('Python %s on %s' % (sys.version, sys.platform))

    users_by_icndcontentset = joblib.load(r'pkl_files/users_by_icndcontentset.pkl')
    similar_users_cut_off = 0.90
    similar_items_cut_off = 0.90


    consumer_news_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_news\consumer_news_viewdata.pkl')
    print('consumer_news_df loaded', len(consumer_news_df))
    construction_news_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_news\construction_news_viewdata.pkl')
    print('construction_news_df loaded', len(construction_news_df))
    pharma_news_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_news\pharma_news_viewdata.pkl')
    print('pharma_news_df loaded', len(pharma_news_df))

    consumer_news_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_news\consumer_news_Usimilarity.pkl')
    print('consumer_news_user_item_filtering_df loaded', len(consumer_news_user_item_filtering_df))
    construction_news_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_news\construction_news_Usimilarity.pkl')
    print('construction_news_user_item_filtering_df loaded', len(construction_news_user_item_filtering_df))
    pharma_news_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_news\pharma_news_Usimilarity.pkl')
    print('pharma_news_user_item_filtering_df loaded', len(pharma_news_user_item_filtering_df))


    consumer_reports_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_reports\consumer_reports_viewdata.pkl')
    print('consumer_reports_df loaded', len(consumer_reports_df))
    consumer_reports_df.rename(columns={'recordId':'Record Id'},inplace=True)
    construction_reports_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_reports\construction_reports_viewdata.pkl')
    print('construction_reports_df loaded', len(construction_reports_df))
    construction_reports_df.rename(columns={'recordId': 'Record Id'}, inplace=True)
    pharma_reports_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_reports\pharma_reports_viewdata.pkl')
    print('pharma_reports_df loaded', len(pharma_reports_df))
    pharma_reports_df.rename(columns={'recordId': 'Record Id'}, inplace=True)

    consumer_reports_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_reports\consumer_reports_Usimilarity.pkl')
    print('consumer_reports_user_item_filtering_df loaded', len(consumer_reports_user_item_filtering_df))
    construction_reports_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_reports\construction_reports_Usimilarity.pkl')
    print('construction_reports_user_item_filtering_df loaded', len(construction_reports_user_item_filtering_df))
    pharma_reports_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_reports\pharma_reports_Usimilarity.pkl')
    print('pharma_reports_user_item_filtering_df loaded', len(pharma_reports_user_item_filtering_df))

    consumer_deals_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_deals\consumer_deals_viewdata.pkl')
    print('consumer_deals_df loaded', len(consumer_deals_df))
    consumer_deals_df.rename(columns={'recordId':'Record Id'},inplace=True)
    construction_deals_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_deals\construction_deals_viewdata.pkl')
    print('construction_deals_df loaded', len(construction_deals_df))
    construction_deals_df.rename(columns={'recordId': 'Record Id'}, inplace=True)
    pharma_deals_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_deals\pharma_deals_viewdata.pkl')
    print('pharma_deals_df loaded', len(pharma_deals_df))
    pharma_deals_df.rename(columns={'recordId': 'Record Id'}, inplace=True)

    consumer_deals_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_deals\consumer_deals_Usimilarity.pkl')
    print('consumer_deals_user_item_filtering_df loaded', len(consumer_deals_user_item_filtering_df))
    construction_deals_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_deals\construction_deals_Usimilarity.pkl')
    print('construction_deals_user_item_filtering_df loaded', len(construction_deals_user_item_filtering_df))
    pharma_deals_user_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_deals\pharma_deals_Usimilarity.pkl')
    print('pharma_deals_user_item_filtering_df loaded', len(pharma_deals_user_item_filtering_df))

    consumer_news_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_news\consumer_news_mergeddata.pkl')
    print('consumer_news_with_metadata_df loaded', len(consumer_news_with_metadata_df))
    construction_news_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_news\construction_news_mergeddata.pkl')
    print('construction_news_with_metadata_df loaded', len(construction_news_with_metadata_df))
    pharma_news_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_news\pharma_news_mergeddata.pkl')
    print('pharma_news_with_metadata_df loaded', len(pharma_news_with_metadata_df))

    consumer_news_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_news\consumer_news_Isimilarity.pkl')
    print('consumer_news_item_item_filtering_df loaded', len(consumer_news_item_item_filtering_df))
    construction_news_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_news\construction_news_Isimilarity.pkl')
    print('construction_news_item_item_filtering_df loaded', len(construction_news_item_item_filtering_df))
    pharma_news_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_news\pharma_news_Isimilarity.pkl')
    print('pharma_news_item_item_filtering_df loaded', len(pharma_news_item_item_filtering_df))

    consumer_reports_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_reports\consumer_reports_mergeddata.pkl')
    print('consumer_reports_with_metadata_df loaded', len(consumer_reports_with_metadata_df))
    consumer_reports_with_metadata_df.rename(columns={'recordId':'Record Id'},inplace=True)
    construction_reports_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_reports\construction_reports_mergeddata.pkl')
    print('construction_reports_with_metadata_df loaded', len(construction_reports_with_metadata_df))
    construction_reports_with_metadata_df.rename(columns={'recordId': 'Record Id'}, inplace=True)
    pharma_reports_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_reports\pharma_reports_mergeddata.pkl')
    print('pharma_reports_with_metadata_df loaded', len(pharma_reports_with_metadata_df))
    pharma_reports_with_metadata_df.rename(columns={'recordId': 'Record Id'}, inplace=True)

    consumer_reports_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_reports\consumer_reports_Isimilarity.pkl')
    print('consumer_reports_item_item_filtering_df loaded', len(consumer_reports_item_item_filtering_df))
    construction_reports_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_reports\construction_reports_Isimilarity.pkl')
    print('construction_reports_item_item_filtering_df loaded', len(construction_reports_item_item_filtering_df))
    pharma_reports_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_reports\pharma_reports_Isimilarity.pkl')
    print('pharma_reports_item_item_filtering_df loaded', len(pharma_reports_item_item_filtering_df))

    consumer_deals_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_deals\consumer_deals_mergeddata.pkl')
    print('consumer_deals_with_metadata_df loaded', len(consumer_deals_with_metadata_df))
    consumer_deals_with_metadata_df.rename(columns={'recordId':'Record Id'},inplace=True)
    construction_deals_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_deals\construction_deals_mergeddata.pkl')
    print('construction_deals_with_metadata_df loaded', len(construction_deals_with_metadata_df))
    construction_deals_with_metadata_df.rename(columns={'recordId': 'Record Id'}, inplace=True)
    pharma_deals_with_metadata_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_deals\pharma_deals_mergeddata.pkl')
    print('pharma_deals_with_metadata_df loaded', len(pharma_deals_with_metadata_df))
    pharma_deals_with_metadata_df.rename(columns={'recordId': 'Record Id'}, inplace=True)

    consumer_deals_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\consumer_deals\consumer_deals_Isimilarity.pkl')
    print('consumer_deals_item_item_filtering_df loaded', len(consumer_deals_item_item_filtering_df))
    construction_deals_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\construction_deals\construction_deals_Isimilarity.pkl')
    print('construction_deals_item_item_filtering_df loaded', len(construction_deals_item_item_filtering_df))
    pharma_deals_item_item_filtering_df = pd.read_pickle(r'C:\Users\krajasekhara\PycharmProjects\apis\recommendations-api\outputs\pharma_deals\pharma_deals_Isimilarity.pkl')
    print('pharma_deals_item_item_filtering_df loaded', len(pharma_deals_item_item_filtering_df))

    def user_viewed_records(ic_content_df,user_id_of_interest,view_columns):

        user_viewed_records_df = ic_content_df[ic_content_df['UserProfileId'] == user_id_of_interest][view_columns]

        user_viewed_records_df.drop_duplicates(inplace=True, ignore_index=True)

        user_viewed_records_df = user_viewed_records_df.sort_values(by=['Viewed Date'], ascending=False).reset_index(drop=True)

        user_viewed_records_list = user_viewed_records_df['Record Id'].unique()

        return [user_viewed_records_df,user_viewed_records_list]


    def user_item_filtering_based_recommendations(ic_content_user_item_filtering_df, ic_content_df,
                                                  user_viewed_records_list, user, cut_off):

        print(user)

        if user in ic_content_user_item_filtering_df.columns.tolist():

            similar_users_ge_cut_off_series = ic_content_user_item_filtering_df[user][ic_content_user_item_filtering_df[user] >= cut_off].sort_values(ascending=False)

            if len(similar_users_ge_cut_off_series) == 0:

                msg_to_carry = f'No similar users above pre-defined cut-off of {cut_off}, cannot recommend by user item filtering. Cut off needs to be lowered'

                print(f'No similar users above pre-defined cut-off of {cut_off}, cannot recommend by user item filtering. Cut off needs to be lowered')

                return [msg_to_carry, pd.DataFrame()]

            else:

                print(len(similar_users_ge_cut_off_series), 'similar_users_above_cut_off')

                if len(similar_users_ge_cut_off_series) < 10:

                    print("yes, less than 10")

                    final_similar_users = similar_users_ge_cut_off_series.index.to_list()

                else:

                    print("yes, more than or eq to 10")

                    final_similar_users = similar_users_ge_cut_off_series[:10].index.to_list()

                similar_users_viewed_records = ic_content_df[ic_content_df['UserProfileId'].isin(final_similar_users)]['Record Id'].unique()

                print(len(similar_users_viewed_records), 'similar_users_viewed_records')

                similar_users_viewed_records_post_eliminating_already_viewed = [record for record in
                                                                                similar_users_viewed_records if
                                                                                record not in user_viewed_records_list]

                print(len(similar_users_viewed_records_post_eliminating_already_viewed),'similar_users_viewed_records_post_eliminating_already_viewed')

                user_item_filtering_recommendations_df = ic_content_df[ic_content_df['Record Id'].isin(similar_users_viewed_records_post_eliminating_already_viewed)][['Record Id', 'Record Title', 'Publication date']].drop_duplicates(keep='first').sort_values(by=['Publication date'], ascending=False).reset_index(drop=True)

                print(len(user_item_filtering_recommendations_df), 'user_item_filtering_recommendations')

                msg_to_carry = f'No.of similar users greater than or equal to cut-off of {cut_off} are {len(similar_users_ge_cut_off_series)}. Recommendations are by top 10 recently published articles post aggregation from top 10 (if >10) similar users.'

                top = 10

                return [msg_to_carry, user_item_filtering_recommendations_df.iloc[:top, :]]

        else:

            msg_to_carry = 'User not in user-user similarity matirx (which is built as part of the process developed); very less view history in last 1 year, recommendations by user item filtering not possible'

            print('User not in user-user similarity matirx (which is built as part of the process developed); very less view history in last 1 year, recommendations by user item filtering not possible')

            return [msg_to_carry, pd.DataFrame()]


    def item_item_filtering_based_recommendations(ic_content_item_item_filtering_df, ic_content_df,
                                                  ic_content_with_metadata_df, user_viewed_records_list, user, cut_off):

        print(user)

        similar_items_list = []

        items_by_user = ic_content_with_metadata_df[ic_content_with_metadata_df['Record Id'].isin(user_viewed_records_list)]['dvalName'].value_counts().head(10).index.tolist()

        print(len(items_by_user))

        items_by_user_filtered = [item for item in items_by_user if
                                  item in ic_content_item_item_filtering_df.columns.tolist()]

        print(len(items_by_user_filtered))

        if len(items_by_user_filtered) > 0:

            print(items_by_user_filtered)

            for item in items_by_user_filtered:

                print('*' * 80)
                print(item)

                similar_items_ge_cut_off_series_for_an_item = ic_content_item_item_filtering_df[item][ic_content_item_item_filtering_df[item] >= cut_off].sort_values(ascending=False)

                if len(similar_items_ge_cut_off_series_for_an_item) > 0:

                    print(len(similar_items_ge_cut_off_series_for_an_item), 'similar_items_ge_cut_off_series_for_an_item')

                    if len(similar_items_ge_cut_off_series_for_an_item) < 10:

                        print("yes, less than 10")

                        similar_items_list.extend(similar_items_ge_cut_off_series_for_an_item.index.to_list())

                    else:

                        print("yes, more than or eq to 10")

                        similar_items_list.extend(similar_items_ge_cut_off_series_for_an_item[:10].index.to_list())

            if len(similar_items_list) == 0:

                msg_to_carry = f'No similar items above pre-defined cut-off of {cut_off}, cannot recommend by item item filtering. Cut off needs to be lowered'

                print(f'No similar items above pre-defined cut-off of {cut_off}, cannot recommend by item item filtering. Cut off needs to be lowered')

                return [msg_to_carry, pd.DataFrame()]

            else:

                print(similar_items_list)

                similar_items_tagged_records = ic_content_with_metadata_df[ic_content_with_metadata_df['dvalName'].isin(similar_items_list)]['Record Id'].unique()

                print(len(similar_items_tagged_records), 'similar_items_tagged_records')

                similar_items_tagged_records_post_eliminating_already_viewed = [record for record in
                                                                                similar_items_tagged_records if
                                                                                record not in user_viewed_records_list]

                print(len(similar_items_tagged_records_post_eliminating_already_viewed),'similar_items_tagged_records_post_eliminating_already_viewed')

                item_item_filtering_recommendations_df = ic_content_df[ic_content_df['Record Id'].isin(similar_items_tagged_records_post_eliminating_already_viewed)][['Record Id', 'Record Title', 'Publication date']].drop_duplicates(keep='first').sort_values(by=['Publication date'], ascending=False).reset_index(drop=True)

                print(len(item_item_filtering_recommendations_df), 'item_item_filtering_recommendations_df')

                msg_to_carry = f'No.of similar items greater than or equal to cut-off of {cut_off} are {len(similar_items_list)}. Recommendations are by top 30 recently published articles post aggregation from all similar items with top 10 user interested items (by frequency).'

                top = 30

                return [msg_to_carry, item_item_filtering_recommendations_df.iloc[:top, :]]

        else:

            msg_to_carry = 'Any of the items not in item-item similarity matirx (which is built as part of the process developed);  recommendations by item item filtering not possible'

            print('Any of the items not in item-item similarity matirx (which is built as part of the process developed);  recommendations by item item filtering not possible')

            return [msg_to_carry, pd.DataFrame()]


    parser = argparse.ArgumentParser()
    parser.add_argument("--port",
                        default=2310,
                        type=int,
                        help="Port to be used for the Flask Application.")
    args = parser.parse_args()
    port = int(args.port)

    app.run(host=host,port=port,debug=True, threaded=True)